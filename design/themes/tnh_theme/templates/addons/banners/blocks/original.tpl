{** block-description:original **}
<div id="owl-main" class="owl-carousel owl-inner-nav owl-ui-sm">
{foreach from=$items item="banner" key="key"}
    {if $banner.type == "G" && $banner.main_pair.image_id}
{*        <pre>{$banner|@print_r}</pre>*}
    <div class="item" style="background-image: url('{$banner.main_pair.icon.image_path}');">
        <div class="container">
            <div class="row">
                <div class="col-md-offset-3 col-md-5">
                    <div class="caption vertical-center text-left">
                        <div class="hero-1 fadeInDown-1">
                            {$banner.banner}
                        </div>

                        <div class="hero-subtitle fadeInDown-2">
                            {$banner.description|strip_tags}
                        </div>
                        {*<div class="hero-v2-price fadeInDown-3">
                            from <br><span>$749</span>
                        </div>*}
                        <div class="hero-action-btn fadeInDown-4">
                            <a href="{$banner.url|fn_url}" class="big le-button ">Start Buying</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {/if}
{/foreach}
</div>