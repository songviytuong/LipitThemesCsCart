{** block-description:tmpl_subscription **}
{if $addons.newsletters}
<div class="footer-newsletter">
    <div class="container">
            <div class="row">
                    <div class="col-xs-12 col-sm-7">
                            <h5 class="newsletter-title">Sign up to Newsletter</h5>
                            <span class="newsletter-marketing-text">...and receive <strong>$20 coupon for first shopping</strong></span>
                    </div>
                    <div class="col-xs-12 col-sm-5">
                            <form action="{""|fn_url}" method="post" name="subscribe_form">
                                    <div class="input-group">
                                            <input type="text" name="subscribe_email" id="subscr_email{$block.block_id}" class="form-control" placeholder='{__("enter_your_email_address")}'/>
                                            <span class="input-group-btn">
                                                    {include file="buttons/go.tpl" but_name="newsletters.add_subscriber" but_text=__("sign_up") alt=__("go")}
                                            </span>
                                    </div>
                            </form>
                    </div>
            </div>
    </div>
</div>
{*<div class="ty-footer-form-block">
    <form action="{""|fn_url}" method="post" name="subscribe_form">
        <input type="hidden" name="redirect_url" value="{$config.current_url}" />
        <input type="hidden" name="newsletter_format" value="2" />
        <h3 class="ty-footer-form-block__title">{__("stay_connected")}</h3>
        <div class="ty-footer-form-block__form ty-control-group ty-input-append">
            <label class="cm-required cm-email hidden" for="subscr_email{$block.block_id}">{__("email")}</label>
            <input type="text" name="subscribe_email" id="subscr_email{$block.block_id}" size="20" value="{__("enter_email")}" class="cm-hint ty-input-text" />
            {include file="buttons/go.tpl" but_name="newsletters.add_subscriber" alt=__("go")}
        </div>
    </form>
</div>*}
{/if}