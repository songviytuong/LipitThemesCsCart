{** block-description:tmpl_call_request **}
<nav>
    <ul id="menu-top-bar-left" class="nav nav-inline pull-left animate-dropdown flip">
        <li class="menu-item animate-dropdown">{__("call_request.work_time")}</li>
        <li class="menu-item animate-dropdown">{$obj_prefix = 'block'}
        {$obj_id = $block.snapping_id|default:0}
        {include file="common/popupbox.tpl"
            href="call_requests.request?obj_prefix={$obj_prefix}&obj_id={$obj_id}"
            link_text=__("call_requests.request_call")
            text=__("call_requests.request_call")
            id="call_request_{$obj_prefix}{$obj_id}"
            link_icon = "ec ec-phone"
            content=""
            icon_align="left"
        }</li>
    </ul>
</nav>
