{if $breadcrumbs && $breadcrumbs|@sizeof > 1}
<nav id="breadcrumbs_{$block.block_id}" class="woocommerce-breadcrumb" >
    {strip}
        {foreach from=$breadcrumbs item="bc" name="bcn" key="key"}
            {if $key != "0"}
                <span class="delimiter"><i class="fa fa-angle-right"></i></span>
            {/if}
            {if $bc.link}
                <a href="{$bc.link|fn_url}" class="ty-breadcrumbs__a{if $additional_class} {$additional_class}{/if}"{if $bc.nofollow} rel="nofollow"{/if}>{$bc.title|strip_tags|escape:"html" nofilter}</a>
            {else}
                {$bc.title|strip_tags|escape:"html" nofilter}
            {/if}
        {/foreach}
        {include file="common/view_tools.tpl"}
    {/strip}
</nav>
{/if}