{assign var="id" value=$id|default:"main_login"}
<h2>{__("login")}</h2>
{capture name="login"}
    <form name="{$id}_form" class="login" action="{""|fn_url}" method="post">
    <input type="hidden" name="return_url" value="{$smarty.request.return_url|default:$config.current_url}" />
    <input type="hidden" name="redirect_url" value="{$config.current_url}" />
        <p class="before-login-text">
            {__("before_login_text")}
        </p>
        {if $style == "checkout"}
            <div class="ty-checkout-login-form">{include file="common/subheader.tpl" title=__("returning_customer")}
        {/if}
        <p class="form-row form-row-wide">
            <label for="psw_{$id}" class="cm-required">{__("email")}</label>
            <input type="text" class="input-text" name="user_login" id="login_{$id}" value="" />
        </p>
        
        <p class="form-row form-row-wide">
            <label for="password" class="cm-required">{__("password")}</label><a href="{"auth.recover_password"|fn_url}" style="float:right" tabindex="5">{__("forgot_password_question")}</a>
            <input class="input-text" type="password" name="password" id="psw_{$id}" />
        </p>

        {if $style == "popup"}
            <div class="ty-login-reglink ty-center">
                <a class="ty-login-reglink__a" href="{"profiles.add"|fn_url}" rel="nofollow">{__("register_new_account")}</a>
            </div>
        {/if}
        

        {include file="common/image_verification.tpl" option="login" align="left"}

        {if $style == "checkout"}
            </div>
        {/if}

        {hook name="index:login_buttons"}
        <p class="form-row">
            {include file="buttons/login.tpl" but_meta="button" but_name="dispatch[auth.login]" but_role="submit"}
            <label for="rememberme" class="inline">
                    <input name="remember_me" id="rememberme" type="checkbox" value="forever" /> Remember me
            </label>
        </p>
        {/hook}
    </form>
{/capture}

{if $style == "popup"}
    {$smarty.capture.login nofilter}
{else}
    {$smarty.capture.login nofilter}
    {capture name="mainbox_title"}{__("sign_in")}{/capture}
{/if}
