{** block-description:three_banner **}
<div class="home-v1-ads-block animate-in-view fadeIn animated" data-animation="fadeIn">
                    <div class="ads-block row">
{foreach from=$items item="banner" key="key"}
    {if $banner.type == "G" && $banner.main_pair.image_id}
{*        <pre>{$banner|@print_r}</pre>*}
    <div class="ad col-xs-12 col-sm-4">
        <div class="media">
            <div class="media-left media-middle">
                <img data-echo="{$banner.main_pair.icon.image_path}" src="assets/images/blank.gif" alt="">
            </div>
            <div class="media-body media-middle">
                <div class="ad-text">
                    {$banner.banner}
                </div>
                <div class="ad-action">
                    <a href="{$banner.url|fn_url}">Shop now</a>
                </div>
            </div>
        </div>
    </div>
    {/if}
{/foreach}
</div>
</div>