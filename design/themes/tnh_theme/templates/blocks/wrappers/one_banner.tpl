{** block-description:one_banner **}
<div class="home-v1-fullbanner-ad fullbanner-ad" style="margin-bottom: 70px">
{foreach from=$items item="banner" key="key"}
    {if $banner.type == "G" && $banner.main_pair.image_id}
    <a href="{$banner.url|fn_url}">
        <img src="assets/images/blank.gif" data-echo="{$banner.main_pair.icon.image_path}" class="img-responsive" alt="{$banner.alt}">
    </a>
    {/if}
{/foreach}
</div>