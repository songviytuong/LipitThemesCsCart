{** block-description:products_tabs_on_sale **}
<div class="tab-pane" id="tab-products-2" role="tabpanel">
                                    <div class="woocommerce columns-3">
                                        <ul class="products columns-3">

                                            <li class="product first">
                                                <div class="product-outer">
                                                    <div class="product-inner">
                                                        <span class="loop-product-categories"><a href="product-category.html" rel="tag">Smartphones</a></span>
                                                        <a href="single-product.html">
                                                            <h3>Notebook Black Spire V Nitro  VN7-591G</h3>
                                                            <div class="product-thumbnail">

                                                                <img data-echo="assets/images/products/4.jpg" src="assets/images/blank.gif" alt="">

                                                            </div>
                                                        </a>

                                                        <div class="price-add-to-cart">
                                                            <span class="price">
                                                                <span class="electro-price">
                                                                    <ins><span class="amount">&#036;1,999.00</span></ins>
                                                                    <del><span class="amount">&#036;2,299.00</span></del>
                                                                </span>
                                                            </span>
                                                            <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">Add to cart</a>
                                                        </div><!-- /.price-add-to-cart -->

                                                        <div class="hover-area">
                                                            <div class="action-buttons">

                                                                <a href="#" rel="nofollow" class="add_to_wishlist">
                                                                    Wishlist</a>

                                                                <a href="#" class="add-to-compare-link">Compare</a>
                                                            </div>
                                                        </div>
                                                    </div><!-- /.product-inner -->
                                                </div><!-- /.product-outer -->
                                            </li>

                                            <li class="product ">
                                                <div class="product-outer">
                                                    <div class="product-inner">
                                                        <span class="loop-product-categories"><a href="product-category.html" rel="tag">Smartphones</a></span>
                                                        <a href="single-product.html">
                                                            <h3>Notebook Black Spire V Nitro  VN7-591G</h3>
                                                            <div class="product-thumbnail">

                                                                <img data-echo="assets/images/products/6.jpg" src="assets/images/blank.gif" alt="">

                                                            </div>
                                                        </a>

                                                        <div class="price-add-to-cart">
                                                            <span class="price">
                                                                <span class="electro-price">
                                                                    <ins><span class="amount">&#036;1,999.00</span></ins>
                                                                    <del><span class="amount">&#036;2,299.00</span></del>
                                                                </span>
                                                            </span>
                                                            <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">Add to cart</a>
                                                        </div><!-- /.price-add-to-cart -->

                                                        <div class="hover-area">
                                                            <div class="action-buttons">

                                                                <a href="#" rel="nofollow" class="add_to_wishlist">
                                                                    Wishlist</a>

                                                                <a href="#" class="add-to-compare-link">Compare</a>
                                                            </div>
                                                        </div>
                                                    </div><!-- /.product-inner -->
                                                </div><!-- /.product-outer -->
                                            </li>

                                            <li class="product last">
                                                <div class="product-outer">
                                                    <div class="product-inner">
                                                        <span class="loop-product-categories"><a href="product-category.html" rel="tag">Smartphones</a></span>
                                                        <a href="single-product.html">
                                                            <h3>Notebook Black Spire V Nitro  VN7-591G</h3>
                                                            <div class="product-thumbnail">

                                                                <img data-echo="assets/images/products/1.jpg" src="assets/images/blank.gif" alt="">

                                                            </div>
                                                        </a>

                                                        <div class="price-add-to-cart">
                                                            <span class="price">
                                                                <span class="electro-price">
                                                                    <ins><span class="amount">&#036;1,999.00</span></ins>
                                                                    <del><span class="amount">&#036;2,299.00</span></del>
                                                                </span>
                                                            </span>
                                                            <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">Add to cart</a>
                                                        </div><!-- /.price-add-to-cart -->

                                                        <div class="hover-area">
                                                            <div class="action-buttons">

                                                                <a href="#" rel="nofollow" class="add_to_wishlist">
                                                                    Wishlist</a>

                                                                <a href="#" class="add-to-compare-link">Compare</a>
                                                            </div>
                                                        </div>
                                                    </div><!-- /.product-inner -->
                                                </div><!-- /.product-outer -->
                                            </li>

                                            <li class="product first">
                                                <div class="product-outer">
                                                    <div class="product-inner">
                                                        <span class="loop-product-categories"><a href="product-category.html" rel="tag">Smartphones</a></span>
                                                        <a href="single-product.html">
                                                            <h3>Notebook Black Spire V Nitro  VN7-591G</h3>
                                                            <div class="product-thumbnail">

                                                                <img data-echo="assets/images/products/2.jpg" src="assets/images/blank.gif" alt="">

                                                            </div>
                                                        </a>

                                                        <div class="price-add-to-cart">
                                                            <span class="price">
                                                                <span class="electro-price">
                                                                    <ins><span class="amount">&#036;1,999.00</span></ins>
                                                                    <del><span class="amount">&#036;2,299.00</span></del>
                                                                </span>
                                                            </span>
                                                            <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">Add to cart</a>
                                                        </div><!-- /.price-add-to-cart -->

                                                        <div class="hover-area">
                                                            <div class="action-buttons">

                                                                <a href="#" rel="nofollow" class="add_to_wishlist">
                                                                    Wishlist</a>

                                                                <a href="#" class="add-to-compare-link">Compare</a>
                                                            </div>
                                                        </div>
                                                    </div><!-- /.product-inner -->
                                                </div><!-- /.product-outer -->
                                            </li>

                                            <li class="product ">
                                                <div class="product-outer">
                                                    <div class="product-inner">
                                                        <span class="loop-product-categories"><a href="product-category.html" rel="tag">Smartphones</a></span>
                                                        <a href="single-product.html">
                                                            <h3>Notebook Black Spire V Nitro  VN7-591G</h3>
                                                            <div class="product-thumbnail">

                                                                <img data-echo="assets/images/products/5.jpg" src="assets/images/blank.gif" alt="">

                                                            </div>
                                                        </a>

                                                        <div class="price-add-to-cart">
                                                            <span class="price">
                                                                <span class="electro-price">
                                                                    <ins><span class="amount">&#036;1,999.00</span></ins>
                                                                    <del><span class="amount">&#036;2,299.00</span></del>
                                                                </span>
                                                            </span>
                                                            <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">Add to cart</a>
                                                        </div><!-- /.price-add-to-cart -->

                                                        <div class="hover-area">
                                                            <div class="action-buttons">

                                                                <a href="#" rel="nofollow" class="add_to_wishlist">
                                                                    Wishlist</a>

                                                                <a href="#" class="add-to-compare-link">Compare</a>
                                                            </div>
                                                        </div>
                                                    </div><!-- /.product-inner -->
                                                </div><!-- /.product-outer -->
                                            </li>

                                            <li class="product last">
                                                <div class="product-outer">
                                                    <div class="product-inner">
                                                        <span class="loop-product-categories"><a href="product-category.html" rel="tag">Smartphones</a></span>
                                                        <a href="single-product.html">
                                                            <h3>Notebook Black Spire V Nitro  VN7-591G</h3>
                                                            <div class="product-thumbnail">

                                                                <img data-echo="assets/images/products/3.jpg" src="assets/images/blank.gif" alt="">

                                                            </div>
                                                        </a>

                                                        <div class="price-add-to-cart">
                                                            <span class="price">
                                                                <span class="electro-price">
                                                                    <ins><span class="amount">&#036;1,999.00</span></ins>
                                                                    <del><span class="amount">&#036;2,299.00</span></del>
                                                                </span>
                                                            </span>
                                                            <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">Add to cart</a>
                                                        </div><!-- /.price-add-to-cart -->

                                                        <div class="hover-area">
                                                            <div class="action-buttons">

                                                                <a href="#" rel="nofollow" class="add_to_wishlist">
                                                                    Wishlist</a>

                                                                <a href="#" class="add-to-compare-link">Compare</a>
                                                            </div>
                                                        </div>
                                                    </div><!-- /.product-inner -->
                                                </div><!-- /.product-outer -->
                                            </li>
                                        </ul>
                                    </div>
                                </div>
{*{$block.user_class}
{$block.properties.item_number}
{foreach from=$items item="pro"}
    <br/>- {$pro.product}
{/foreach}*}
