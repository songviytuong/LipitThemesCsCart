{** block-description:products_main_212 **}
<ul class="products exclude-auto-height product-main-2-1-2">
                                <li class="last product">
                                    <div class="product-outer">
                                        <div class="product-inner">
                                            <span class="loop-product-categories"><a href="product-category.html" rel="tag">Smartphones</a></span>
                                            <a href="single-product.html">
                                                <h3>Notebook Black Spire V Nitro  VN7-591G</h3>
                                                <div class="product-thumbnail">
                                                    <img class="wp-post-image" data-echo="assets/images/products/main.jpg" src="assets/images/blank.gif" alt="">

                                                </div>
                                            </a>

                                            <div class="price-add-to-cart">
                                                <span class="price">
                                                    <span class="electro-price">
                                                        <ins><span class="amount">&#036;1,999.00</span></ins>
                                                        <del><span class="amount">&#036;2,299.00</span></del>
                                                    </span>
                                                </span>
                                                <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">Add to cart</a>
                                            </div><!-- /.price-add-to-cart -->

                                            <div class="hover-area">
                                                <div class="action-buttons">

                                                    <a href="#" rel="nofollow" class="add_to_wishlist">
                                                        Wishlist</a>

                                                    <a href="#" class="add-to-compare-link">Compare</a>
                                                </div>
                                            </div>
                                        </div><!-- /.product-inner -->
                                    </div><!-- /.product-outer -->
                                </li>
                            </ul>
{*{$block.user_class}
{$block.properties.item_number}
{foreach from=$items item="pro"}
    <br/>- {$pro.product}
{/foreach}*}
