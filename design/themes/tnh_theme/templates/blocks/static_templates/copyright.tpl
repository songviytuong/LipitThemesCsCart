{** block-description:tmpl_copyright **}
<div class="copyright-bar">
        <div class="container">
                <div class="pull-left flip copyright">&copy; {if $smarty.const.TIME|date_format:"%Y" != $settings.Company.company_start_year}{$settings.Company.company_start_year}-{/if}{$smarty.const.TIME|date_format:"%Y"} {$settings.Company.company_name}. &nbsp;{__("powered_by")} <a class="bottom-copyright" href="{$config.resources.product_url}" target="_blank">{__("copyright_shopping_cart", ["[product]" => $smarty.const.PRODUCT_NAME])}</a> - All Rights Reserved</div>
                <div class="pull-right flip payment">
                        <div class="footer-payment-logo">
                                <ul class="cash-card card-inline">
                                        <li class="card-item"><img src="assets/images/footer/payment-icon/1.png" alt="" width="52"></li>
                                        <li class="card-item"><img src="assets/images/footer/payment-icon/2.png" alt="" width="52"></li>
                                        <li class="card-item"><img src="assets/images/footer/payment-icon/3.png" alt="" width="52"></li>
                                        <li class="card-item"><img src="assets/images/footer/payment-icon/4.png" alt="" width="52"></li>
                                        <li class="card-item"><img src="assets/images/footer/payment-icon/5.png" alt="" width="52"></li>
                                </ul>
                        </div>
                </div>
        </div>
</div>