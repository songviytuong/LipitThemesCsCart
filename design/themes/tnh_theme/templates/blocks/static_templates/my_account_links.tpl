<nav>
    <ul id="menu-top-bar-right account_info_links_{$block.snapping_id}" class="nav nav-inline pull-right animate-dropdown flip">
        <li class="menu-item animate-dropdown"><a title="Store Locator" href="#"><i class="ec ec-map-pointer"></i>Store Locator</a></li>
        <li class="menu-item animate-dropdown"><a title="Track Your Order" href="track-your-order.html"><i class="ec ec-transport"></i>Track Your Order</a></li>
        <li class="menu-item animate-dropdown"><a title="Shop" href="shop.html"><i class="ec ec-shopping-bag"></i>Shop</a></li>
        {if $auth.user_id}
        <li class="menu-item animate-dropdown"><a href="{"orders.search"|fn_url}">{__("orders")}</a></li>
        <li class="menu-item animate-dropdown"><a href="{"profiles.update"|fn_url}"><i class="ec ec-user"></i>{__("profile_details")}</a></li>
        {else}
        <li class="menu-item animate-dropdown"><a href="{"auth.login_form"|fn_url}" rel="nofollow">{__("sign_in")}</a></li>
        <li class="menu-item animate-dropdown"><a href="{"profiles.add"|fn_url}" rel="nofollow">{__("create_account")}</a></li>
        {/if}
    </ul>
</nav>