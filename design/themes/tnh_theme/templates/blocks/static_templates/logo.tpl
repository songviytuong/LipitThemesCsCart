{** block-description:tmpl_logo **}
<div class="header-logo">
    <a href="{""|fn_url}" class="header-logo-link" title="{$logos.theme.image.alt}">
        <img src="{$logos.theme.image.image_path}" width="{$logos.theme.image.image_x}" height="{$logos.theme.image.image_y}" alt="{$logos.theme.image.alt}"/>
    </a>
</div>