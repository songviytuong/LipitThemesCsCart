{** block-description:main_menu **}

{if $block.properties.show_items_in_line == 'Y'}
    {assign var="inline" value=true}
{/if}

{assign var="text_links_id" value=$block.snapping_id}

{if $items}
    {hook name="blocks:main_menu"}
{*    <pre>{$items|@print_r}</pre>*}
    
    <div class="col-xs-12 col-lg-3">
            <nav>
                <ul class="list-group vertical-menu yamm make-absolute">
                    <li class="list-group-item"><span><i class="fa fa-list-ul"></i> {__("all_departments")}</span></li>

                    {*<li class="highlight menu-item animate-dropdown"><a title="Value of the Day" href="home-v2.html">Value of the Day</a></li>
                    <li class="highlight menu-item animate-dropdown"><a title="Top 100 Offers" href="home-v3.html">Top 100 Offers</a></li>
                    <li class="highlight menu-item animate-dropdown"><a title="New Arrivals" href="home-v3-full-color.html">New Arrivals</a></li>*}
                    
                    {*<li class="yamm-tfw menu-item menu-item-has-children animate-dropdown {if $menu.subitems}dropdown{/if}">
                        <a title="{$menu.item}" data-hover="dropdown" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">{$menu.item}</a>
                        {if $menu.subitems}
                        <ul role="menu" class=" dropdown-menu">
                            <li class="menu-item animate-dropdown menu-item-object-static_block">
                                <div class="yamm-content">
                                    <div class="row bg-yamm-content bg-yamm-content-bottom bg-yamm-content-right">
                                        <div class="col-sm-12">
                                            <div class="vc_column-inner ">
                                                <div class="wpb_wrapper">
                                                    <div class="wpb_single_image wpb_content_element vc_align_left">
                                                        <figure class="wpb_wrapper vc_figure">
                                                            <div class="vc_single_image-wrapper vc_box_border_grey">
                                                                <img src="assets/images/megamenu-2.png" class="vc_single_image-img attachment-full" alt="">
                                                            </div>
                                                        </figure>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="vc_column-inner ">
                                                <div class="wpb_wrapper">
                                                    <div class="wpb_text_column wpb_content_element ">
                                                        <div class="wpb_wrapper">
                                                            <ul>
                                                                {foreach from=$menu.subitems item=sub}
                                                                <li class="nav-title">Computers &amp; Accessories</li>
                                                                <li><a href="#">{$sub.item}</a></li>
                                                                {/foreach}
                                                                <li class="nav-divider"></li>
                                                                <li><a href="#"><span class="nav-text">All Electronics</span><span class="nav-subtext">Discover more products</span></a></li>
                                                            </ul>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="vc_column-inner ">
                                                <div class="wpb_wrapper">
                                                    <div class="wpb_text_column wpb_content_element ">
                                                        <div class="wpb_wrapper">
                                                            <ul>
                                                                <li class="nav-title">Office &amp; Stationery</li>
                                                                <li><a href="#">All Office &amp; Stationery</a></li>
                                                                <li><a href="#">Pens &amp; Writing</a></li>
                                                            </ul>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                        {/if}
                    </li>*}
                   
                    {foreach from=$items item=menu}
                    {assign var="menu_url" value=$menu.href}
                    <li id="menu-item-2695" class="menu-item menu-item-has-children animate-dropdown {if $menu.subitems}dropdown{/if}">
                        <a title="{$menu.item}" data-hover="dropdown" {if $menu_url}href="{$menu_url|fn_url}"{/if} data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">{$menu.item}</a>
                        {if $menu.subitems}
                        <ul role="menu" class=" dropdown-menu">
                            {foreach from=$menu.subitems item=sub}
                            {assign var="sub_url" value=$sub.href}
                            <li class="menu-item animate-dropdown"><a {if $sub_url}href="{$sub_url|fn_url}"{/if} title="{$sub.item}">{$sub.item}</a></li>
                            {/foreach}
                        </ul>
                        {/if}
                    </li>
                    {/foreach}
                </ul>
            </nav>
        </div>
    {*
    {if $inline && !$submenu}
    <div class="ty-text-links-wrapper">
        <span id="sw_text_links_{$text_links_id}" class="ty-text-links-btn cm-combination visible-phone">
            <i class="ty-icon-short-list"></i>
            <i class="ty-icon-down-micro ty-text-links-btn__arrow"></i>
        </span>
    {/if}

        <ul {if !$submenu}id="text_links_{$text_links_id}"{/if} class="ty-text-links{if $inline && !$submenu} cm-popup-box ty-text-links_show_inline{/if}">
            {foreach from=$items item="menu"}
                <li class="ty-text-links__item ty-level-{$menu.level|default:0}{if $menu.active} ty-text-links__active{/if}{if $menu.class} {$menu.class}{/if}{if $inline && !$submenu && $menu.subitems} ty-text-links__subitems{/if}">
                    <a class="ty-text-links__a" {if $menu.href}href="{$menu.href|fn_url}"{/if}>{$menu.item}</a> 
                    {if $menu.subitems}
                        {include file="blocks/menu/text_links.tpl" items=$menu.subitems submenu=true}
                    {/if}
                </li>
            {/foreach}
        </ul>

    {if $inline && !$submenu}
    </div>
    {/if*}
    {/hook}
{/if}