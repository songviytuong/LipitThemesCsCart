{** block-description:secondary_menu **}

{if $block.properties.show_items_in_line == 'Y'}
    {assign var="inline" value=true}
{/if}

{assign var="text_links_id" value=$block.snapping_id}

{if $items}
    <div class="col-xs-12 col-lg-9">
        <nav id='{$text_links_id}'>
            <ul id="menu-secondary-nav" class="secondary-nav">
            {foreach from=$items item=menu}
                <li class="{($menu.class)? $menu.class : ''} menu-item"><a href="{$menu.href}">{$menu.item}</a></li>
            {/foreach}
            </ul>
        </nav>
    </div>
{/if}