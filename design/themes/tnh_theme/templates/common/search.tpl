<form class="navbar-search" action="{""|fn_url}" name="search_form" method="get">
    <label class="sr-only screen-reader-text" for="search">Search for:</label>
    <div class="input-group">
    <input type="hidden" name="subcats" value="Y" />
    <input type="hidden" name="pcode_from_q" value="Y" />
    <input type="hidden" name="pshort" value="Y" />
    <input type="hidden" name="pfull" value="Y" />
    <input type="hidden" name="pname" value="Y" />
    <input type="hidden" name="pkeywords" value="Y" />
    <input type="hidden" name="search_performed" value="Y" />

    {hook name="search:additional_fields"}{/hook}

    {strip}
        {if $settings.General.search_objects}
            {assign var="search_title" value=__("search")}
        {else}
            {assign var="search_title" value=__("search_products")}
        {/if}
        <input type="text" name="q" value="{$search.q}" placeholder="{$search_title}" id="search_input{$smarty.capture.search_input_id}" title="{$search_title}" class="form-control search-field cm-hint" />
    {/strip}
    <div class="input-group-addon search-categories">
        <select name='product_cat' id='product_cat' class='postform resizeselect' >
            <option value='0' selected='selected'>All Categories</option>
            <option class="level-0" value="laptops-laptops-computers">Laptops</option>
            <option class="level-0" value="ultrabooks-laptops-computers">Ultrabooks</option>
            <option class="level-0" value="mac-computers-laptops">Mac Computers</option>
            <option class="level-0" value="all-in-one-laptops-computers">All in One</option>
            <option class="level-0" value="servers">Servers</option>
            <option class="level-0" value="peripherals">Peripherals</option>
            <option class="level-0" value="gaming-laptops-computers">Gaming</option>
            <option class="level-0" value="accessories-laptops-computers">Accessories</option>
            <option class="level-0" value="audio-speakers">Audio Speakers</option>
            <option class="level-0" value="headphones">Headphones</option>
            <option class="level-0" value="computer-cases">Computer Cases</option>
            <option class="level-0" value="printers">Printers</option>
            <option class="level-0" value="cameras">Cameras</option>
            <option class="level-0" value="smartphones">Smartphones</option>
            <option class="level-0" value="game-consoles">Game Consoles</option>
            <option class="level-0" value="power-banks">Power Banks</option>
            <option class="level-0" value="smartwatches">Smartwatches</option>
            <option class="level-0" value="chargers">Chargers</option>
            <option class="level-0" value="cases">Cases</option>
            <option class="level-0" value="headphone-accessories">Headphone Accessories</option>
            <option class="level-0" value="headphone-cases">Headphone Cases</option>
            <option class="level-0" value="tablets">Tablets</option>
            <option class="level-0" value="tvs">TVs</option>
            <option class="level-0" value="wearables">Wearables</option>
            <option class="level-0" value="pendrives">Pendrives</option>
        </select>
    </div>
    <div class="input-group-btn">
        <button type="submit" class="btn btn-secondary"><i class="ec ec-search"></i></button>
    </div>
    {capture name="search_input_id"}{$block.snapping_id}{/capture}
    </div>
</form>
    
    <ul class="navbar-mini-cart navbar-nav animate-dropdown nav pull-right flip">
                            <li class="nav-item dropdown">
                                <a href="cart.html" class="nav-link" data-toggle="dropdown">
                                    <i class="ec ec-shopping-bag"></i>
                                    <span class="cart-items-count count">4</span>
                                    <span class="cart-items-total-price total-price"><span class="amount">&#36;1,215.00</span></span>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-mini-cart">
                                    <li>
                                        <div class="widget_shopping_cart_content">

                                            <ul class="cart_list product_list_widget ">


                                                <li class="mini_cart_item">
                                                    <a title="Remove this item" class="remove" href="#">×</a>
                                                    <a href="single-product.html">
                                                        <img class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" src="assets/images/products/mini-cart1.jpg" alt="">White lumia 9001&nbsp;
                                                    </a>

                                                    <span class="quantity">2 × <span class="amount">£150.00</span></span>
                                                </li>


                                                <li class="mini_cart_item">
                                                    <a title="Remove this item" class="remove" href="#">×</a>
                                                    <a href="single-product.html">
                                                        <img class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" src="assets/images/products/mini-cart2.jpg" alt="">PlayStation 4&nbsp;
                                                    </a>

                                                    <span class="quantity">1 × <span class="amount">£399.99</span></span>
                                                </li>

                                                <li class="mini_cart_item">
                                                    <a data-product_sku="" data-product_id="34" title="Remove this item" class="remove" href="#">×</a>
                                                    <a href="single-product.html">
                                                        <img class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" src="assets/images/products/mini-cart3.jpg" alt="">POV Action Cam HDR-AS100V&nbsp;

                                                    </a>

                                                    <span class="quantity">1 × <span class="amount">£269.99</span></span>
                                                </li>


                                            </ul><!-- end product list -->


                                            <p class="total"><strong>Subtotal:</strong> <span class="amount">£969.98</span></p>


                                            <p class="buttons">
                                                <a class="button wc-forward" href="cart.html">View Cart</a>
                                                <a class="button checkout wc-forward" href="checkout.html">Checkout</a>
                                            </p>


                                        </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>

                        <ul class="navbar-wishlist nav navbar-nav pull-right flip">
                            <li class="nav-item">
                                <a href="wishlist.html" class="nav-link"><i class="ec ec-favorites"></i></a>
                            </li>
                        </ul>
                        <ul class="navbar-compare nav navbar-nav pull-right flip">
                            <li class="nav-item">
                                <a href="compare.html" class="nav-link"><i class="ec ec-compare"></i></a>
                            </li>
                        </ul>