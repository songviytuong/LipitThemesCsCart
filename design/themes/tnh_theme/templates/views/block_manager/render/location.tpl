
{if $containers.top_panel}
    {$containers.top_panel nofilter}
{/if}

{if $containers.header}
<header id="masthead" class="site-header header-v1">
    {$containers.header nofilter}
</header>
{/if}

{if $containers.content}
<div id="content" class="site-content" tabindex="-1">
        <div class="container">
        <div id="primary" class="content-area">
            <main id="main" class="site-main">
                {$containers.content nofilter}
            </main>
        </div>
    </div>
</div>
{/if}


{if $containers.footer}
    {$containers.footer nofilter}
{/if}

{if "ULTIMATE"|fn_allowed_for}
    {* Show "Entry page" *}
    {if $show_entry_page}
        <div id="entry_page"></div>
            <script type="text/javascript">
                $('#entry_page').ceDialog('open', {$ldelim}href: fn_url('companies.entry_page'), resizable: false, title: '{__("choose_your_country")}', width: 325, height: 420, dialogClass: 'entry-page'{$rdelim});
            </script>
    {/if}
{/if}

{if $smarty.request.meta_redirect_url|fn_check_meta_redirect}
    <meta http-equiv="refresh" content="1;url={$smarty.request.meta_redirect_url|fn_check_meta_redirect|fn_url}" />
{/if}